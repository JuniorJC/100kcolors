# README #

This is an Android App that is using the MVP Architecture according to the _Android Architecture Blueprints_ by Google (https://github.com/googlesamples/android-architecture).

The main idea of the app is to demonstrate how to list a 100k itens using some pre-conditions:

```
If the index of an element is divisible without remainder by 3 (THREE) 
    then colorize the element RED.
    
If the index of an element is divisible without remainder by 5 (FIVE) 
    then colorize the element BLUE.
    
If the index of an element is divisible without remainder by 3 (THREE) AND 5 (FIVE) 
    then colorize the element YELLOW.
```

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

1. Android SDK (Version 26 for compile)
2. Android Studio 2.3.3 (With Build Tools 26)

```
https://developer.android.com/studio/index.html
```

### Installing

Access your project folder

```
cd path_to_directory
```

Execute Gradle Wrapper in order to download all dependencies and build the project

```
gradlew build
```

Deploy the .apk into the device

```
gradlew installDebug
```

Search for the app into your device app list and run it manually (Gradle don't start it automatically)


## Built With

* **[Android SDK]** - Android Software Development Kit
https://developer.android.com/studio/index.html

* **[Butterknife]** - Framework to speed up Android development
http://jakewharton.github.io/butterknife/

* **[Guava]** - Is a set of core libraries to help in your daily activities. Its maintained by Google
https://github.com/google/guava

* **[Android Support API]** - Offers backward-compatible for features that are not built into the framework.
https://developer.android.com/topic/libraries/support-library/index.html)

* **[EndlessScrollListener]** - A third-party class that deal with the Endless RecyclerView scrolling 
for different layout managers
https://gist.github.com/nesquena/d09dc68ff07e845cc622


## Acknowledgments

The app was designed to request the information from a remote repository and save it locally for future 
requests. 

Considering that this behavior was out of scope , I decided to use a flavor to mock the remote repository
and return the information that I expected paginated. 


## Author

* **José Carlos Junior**