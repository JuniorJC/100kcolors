package org.bitbucket.juniorjc.a100kcolors;

import android.content.Context;
import android.support.annotation.NonNull;

import org.bitbucket.juniorjc.a100kcolors.data.FakeColorsRemoteDataSource;
import org.bitbucket.juniorjc.a100kcolors.data.color.source.ColorRepository;
import org.bitbucket.juniorjc.a100kcolors.data.color.source.local.ColorLocalDataSource;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Enables injection of mock implementations for
 * {@link org.bitbucket.juniorjc.a100kcolors.data.color.source.ColorDataSource} at compile time.
 * This is useful for testing, since it allows us to use
 * a fake instance of the class to isolate the dependencies and run a test hermetically.
 */
public class Injection {

    public static ColorRepository provideColorsRepository(@NonNull Context context) {
        checkNotNull(context);
        return ColorRepository.getInstance(FakeColorsRemoteDataSource.getInstance(),
                ColorLocalDataSource.getInstance(context));
    }

}
