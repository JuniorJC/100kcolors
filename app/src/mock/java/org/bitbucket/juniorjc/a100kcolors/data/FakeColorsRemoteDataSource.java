package org.bitbucket.juniorjc.a100kcolors.data;

import android.support.annotation.NonNull;

import org.bitbucket.juniorjc.a100kcolors.data.color.Color;
import org.bitbucket.juniorjc.a100kcolors.data.color.source.ColorDataSource;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import util.Constants;


/**
 * Created by JCJunior on 04/08/2017.
 */

public class FakeColorsRemoteDataSource implements ColorDataSource {

    private static FakeColorsRemoteDataSource INSTANCE;

    Map<Integer, List<Color>> mPaginatedColors;

    // Prevent direct instantiation.
    private FakeColorsRemoteDataSource() {
    }

    public static FakeColorsRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FakeColorsRemoteDataSource();
        }
        return INSTANCE;
    }

    @Override
    public void saveColor(@NonNull Color color, SaveColorCallback callback) {
    }

    @Override
    public void getColors(int pageNumber, @NonNull GetColorsCallback callback) {

        // it is the first page
        if (mPaginatedColors == null){

            List<Color> colors = generateColorsList(pageNumber, null);

            if (!colors.isEmpty()){

                mPaginatedColors = new LinkedHashMap<>();
                mPaginatedColors.put(pageNumber, colors);

                callback.onSuccess(mPaginatedColors.get(pageNumber));
                return;

            }

        }

        //page already exist on cache
        if (mPaginatedColors.get(pageNumber) != null){
            callback.onSuccess(mPaginatedColors.get(pageNumber));
            return;

        }

        //second page and so on...
        if (pageNumber != 0) {

            //getting a new page using the previous page (-1) as base
            int lastPageNumber = pageNumber - 1;
            List<Color> lastPage = mPaginatedColors.get(lastPageNumber);
            Color lastPageItem = lastPage.get(lastPage.size() - 1);

            List<Color> colors = generateColorsList(pageNumber, lastPageItem);

            if (!colors.isEmpty()){
                mPaginatedColors.put(pageNumber, colors);
                callback.onSuccess(mPaginatedColors.get(pageNumber));
            }

        }
    }

    private List<Color> generateColorsList(int pageNumber, Color lastPageItem) {

        List<Color> colors = new ArrayList<>();
        
        int limit = Constants.ITENS_PER_PAGE;
        int autoIncrement = 1;

        if (lastPageItem != null) {
            autoIncrement = lastPageItem.getAutoIncrement()+1;
            limit = (pageNumber+1) * Constants.ITENS_PER_PAGE;
        }

        int i = (limit - Constants.ITENS_PER_PAGE) + 1;

        while (i <= limit && autoIncrement <= Constants.INDEX_LIMIT){

            Color color = null;

            if (i % 3 == 0 && i % 5 == 0){
                color = new Color(autoIncrement, i, Constants.YELLOW);
                autoIncrement++;

            } else if (i % 3 == 0){
                color = new Color(autoIncrement, i, Constants.RED);
                autoIncrement++;

            } else if (i % 5 == 0){
                color = new Color(autoIncrement, i, Constants.BLUE);
                autoIncrement++;
            }

            if (color != null){
                colors.add(color);
            }

            i++;

        }

        return colors;
    }
}
