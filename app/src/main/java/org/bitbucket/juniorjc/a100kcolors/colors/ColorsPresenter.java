package org.bitbucket.juniorjc.a100kcolors.colors;

import org.bitbucket.juniorjc.a100kcolors.data.color.Color;
import org.bitbucket.juniorjc.a100kcolors.data.color.source.ColorDataSource;
import org.bitbucket.juniorjc.a100kcolors.data.color.source.ColorRepository;

import java.util.List;

import util.Constants;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by jcjunior on 08/09/2017.
 */

public class ColorsPresenter implements ColorsContract.Presenter {

    private static final String TAG = ColorsPresenter.class.getSimpleName();

    private final ColorRepository mColorRepository;

    private final ColorsContract.View mColorView;

    public ColorsPresenter(ColorRepository mColorRepository, ColorsContract.View mColorView) {
        this.mColorRepository = checkNotNull(mColorRepository, "mColorRepository cannot be null");;
        this.mColorView = checkNotNull(mColorView, "mColorView cannot be null!");

        mColorView.setPresenter(this);
    }

    @Override
    public void start() {
        mColorView.configureRecycleView();
        mColorView.configureAdapter();
    }

    // Append the next page of data into the adapter
    // This method sends out a network request and appends new data items to your adapter.
    @Override
    public void loadNextDataFromApi(int pageNumber, final int totalItemsCount) {

        mColorRepository.getColors(pageNumber, new ColorDataSource.GetColorsCallback() {
            @Override
            public void onSuccess(List<Color> colors) {

                // The view may not be able to handle UI updates anymore
                if (!mColorView.isActive()) {
                    return;
                }

                mColorView.updateAdapter(colors);

            }

            @Override
            public void onError() {
                mColorView.showErrorLoadingColors();
            }
        });


    }

    @Override
    public void loadFirstDataFromApi() {
        loadNextDataFromApi(0,0);
    }
}
