package org.bitbucket.juniorjc.a100kcolors.data.color.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import org.bitbucket.juniorjc.a100kcolors.data.ColorsDbHelper;
import org.bitbucket.juniorjc.a100kcolors.data.color.Color;
import org.bitbucket.juniorjc.a100kcolors.data.color.source.ColorDataSource;
import org.bitbucket.juniorjc.a100kcolors.data.color.source.local.ColorPersistenceContract.ColorEntry;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by jcjunior on 08/09/2017.
 */

public class ColorLocalDataSource implements ColorDataSource {

    private static final String TAG = ColorLocalDataSource.class.getSimpleName();

    private static ColorLocalDataSource INSTANCE;

    private ColorsDbHelper mDbHelper;

    // Prevent direct instantiation.
    private ColorLocalDataSource(@NonNull Context context) {
        checkNotNull(context);
        mDbHelper = new ColorsDbHelper(context);
    }

    public static ColorLocalDataSource getInstance(@NonNull Context context) {
        if (INSTANCE == null) {
            INSTANCE = new ColorLocalDataSource(context);
        }
        return INSTANCE;
    }

    @Override
    public void saveColor(@NonNull Color color, SaveColorCallback callback) {

        checkNotNull(color);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        db.beginTransaction();

        try {

            ContentValues values = new ContentValues();
            values.put(ColorEntry.COLUMN_NAME_ID, color.getAutoIncrement());
            values.put(ColorEntry.COLUMN_NAME_NUMBER, color.getDivBy());
            values.put(ColorEntry.COLUMN_NAME_COLOR_HEX, color.getColorsInHex());

            db.insert(ColorEntry.TABLE_NAME, null, values);

            db.setTransactionSuccessful();
        }
        catch (Exception e) {
            //Error in between database transaction
        }finally {
            db.endTransaction();
            db.close();
        }

    }

    @Override
    public void getColors(int pageNumber, @NonNull GetColorsCallback callback) {

        try {

            SQLiteDatabase db = mDbHelper.getReadableDatabase();

            String[] select = {
                    ColorEntry.COLUMN_NAME_ID,
                    ColorEntry.COLUMN_NAME_NUMBER,
                    ColorEntry.COLUMN_NAME_COLOR_HEX
            };

            Cursor c = db.query(ColorEntry.TABLE_NAME, select, null, null, null, null, null );

            List<Color> colors = new ArrayList<>();

            if (c != null && c.getCount() > 0) {

                while (c.moveToNext()) {

                    Integer id = c.getInt(c.getColumnIndexOrThrow(ColorEntry.COLUMN_NAME_ID));
                    Integer number = c.getInt(c.getColumnIndexOrThrow(ColorEntry.COLUMN_NAME_NUMBER));
                    String colorInHex = c.getString(c.getColumnIndexOrThrow(ColorEntry.COLUMN_NAME_COLOR_HEX));

                    Color color = new Color(id, number , colorInHex);

                    colors.add(color);

                }
            }
            if (c != null) {
                c.close();
            }

            db.close();

            callback.onSuccess(colors);

        } catch (Exception e) {
            callback.onError();
        }

    }
}
