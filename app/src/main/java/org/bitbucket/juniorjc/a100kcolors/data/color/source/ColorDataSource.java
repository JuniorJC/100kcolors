package org.bitbucket.juniorjc.a100kcolors.data.color.source;

import android.support.annotation.NonNull;

import org.bitbucket.juniorjc.a100kcolors.data.color.Color;

import java.util.Collection;
import java.util.List;

/**
 * Created by jcjunior on 08/09/2017.
 */

public interface ColorDataSource {

    interface SaveColorCallback {

        void onSuccess(Color color);

        void onError();
    }

    interface GetColorsCallback {

        void onSuccess(List<Color> colors);

        void onError();
    }

    void saveColor(@NonNull Color color, SaveColorCallback callback);

    void getColors(int pageNumber, @NonNull GetColorsCallback callback);
}
