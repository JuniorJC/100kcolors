package org.bitbucket.juniorjc.a100kcolors;

/**
 * Created by jcjunior on 08/09/2017.
 */

public interface BasePresenter {

    void start();
}
