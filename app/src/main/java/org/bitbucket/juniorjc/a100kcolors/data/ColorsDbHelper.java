package org.bitbucket.juniorjc.a100kcolors.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.bitbucket.juniorjc.a100kcolors.data.color.source.local.ColorPersistenceContract.ColorEntry;

/**
 * Created by jcjunior on 08/09/2017.
 */

public class ColorsDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 2;

    public static final String DATABASE_NAME = "Colors.db";

    private static final String TEXT_TYPE = " TEXT";

    private static final String INTEGER_TYPE = " INTEGER";

    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_COLOR_TABLE =
            "CREATE TABLE " + ColorEntry.TABLE_NAME + " (" +
                    ColorEntry.COLUMN_NAME_ID + INTEGER_TYPE + COMMA_SEP +
                    ColorEntry.COLUMN_NAME_NUMBER + INTEGER_TYPE + COMMA_SEP +
                    ColorEntry.COLUMN_NAME_COLOR_HEX + TEXT_TYPE  +
                    " )";

    public ColorsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            sqLiteDatabase.execSQL(SQL_CREATE_COLOR_TABLE);
        } catch (Exception e) {
            Log.e("Create DB", e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Not required as at version 1
    }
}
