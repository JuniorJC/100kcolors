package org.bitbucket.juniorjc.a100kcolors.colors;

import org.bitbucket.juniorjc.a100kcolors.BasePresenter;
import org.bitbucket.juniorjc.a100kcolors.BaseView;
import org.bitbucket.juniorjc.a100kcolors.data.color.Color;

import java.util.List;

/**
 * Created by jcjunior on 08/09/2017.
 */

public interface ColorsContract {

    interface View extends BaseView<Presenter> {

        void updateAdapter(List<Color> colors);

        void showErrorLoadingColors();

        void configureRecycleView();

        void configureAdapter();
    }

    interface Presenter extends BasePresenter {

        void loadNextDataFromApi(int page, int totalItemsCount);

        void loadFirstDataFromApi();
    }
}
