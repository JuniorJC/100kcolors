package org.bitbucket.juniorjc.a100kcolors.data.color.source.local;

import android.provider.BaseColumns;

/**
 * The contract used for the db to save the colors locally.
 *
 * Created by jcjunior on 08/09/2017.
 */

public class ColorPersistenceContract {

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    private ColorPersistenceContract() {}

    /* Inner class that defines the table contents */
    public static abstract class ColorEntry implements BaseColumns {

        public static final String TABLE_NAME = "color";

        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_NUMBER = "number";
        public static final String COLUMN_NAME_COLOR_HEX = "colorHex";

    }
}
