package org.bitbucket.juniorjc.a100kcolors.colorDetails;

import org.bitbucket.juniorjc.a100kcolors.BasePresenter;
import org.bitbucket.juniorjc.a100kcolors.BaseView;
import org.bitbucket.juniorjc.a100kcolors.data.color.Color;

import java.util.List;

/**
 * Created by jcjunior on 08/09/2017.
 */

public interface ColorDetailsContract {

    interface View extends BaseView<Presenter> {

        void showColorsDetails(Color mColor);

        void showColorsDetailsAbove(Color color);

        void showColorsDetailsBelow(Color color);
    }

    interface Presenter extends BasePresenter {

    }
}
