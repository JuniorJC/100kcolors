package org.bitbucket.juniorjc.a100kcolors.data.color;

import android.support.annotation.IntRange;

import java.io.Serializable;

/**
 * Created by jcjunior on 08/09/2017.
 */

public class Color implements Serializable {

    private int autoIncrement;

    private int divBy;

    /* 3 - red (0xFFFF0000) , 5 - blue (0xFF0000FF) or  3 and 5 - yellow (0xFFFFFF00) */
    private String colorsInHex; // TODO: maybe an enum ?!

    public Color(int autoIncrement, int divBy, String colorsInHex) {
        this.autoIncrement = autoIncrement;
        this.divBy = divBy;
        this.colorsInHex = colorsInHex;
    }

    public Color() {
    }

    public int getAutoIncrement() {
        return autoIncrement;
    }

    public void setAutoIncrement(int autoIncrement) {
        this.autoIncrement = autoIncrement;
    }

    public String getColorsInHex() {
        return colorsInHex;
    }

    public void setColorsInHex(String colorsInHex) {
        this.colorsInHex = colorsInHex;
    }

    public int getDivBy() {
        return divBy;
    }

    public void setDivBy(int divBy) {
        this.divBy = divBy;
    }

}
