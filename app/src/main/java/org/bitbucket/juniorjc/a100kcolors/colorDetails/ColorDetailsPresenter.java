package org.bitbucket.juniorjc.a100kcolors.colorDetails;

import org.bitbucket.juniorjc.a100kcolors.data.color.Color;
import org.bitbucket.juniorjc.a100kcolors.data.color.source.ColorRepository;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by jcjunior on 08/09/2017.
 */

public class ColorDetailsPresenter implements ColorDetailsContract.Presenter {

    private static final String TAG = ColorDetailsPresenter.class.getSimpleName();

    private final ColorRepository mColorRepository;

    private final ColorDetailsContract.View mColorDetailView;

    private List<Color> mColors;

    public ColorDetailsPresenter(List<Color> colors, ColorRepository mColorRepository, ColorDetailsContract.View mColorDetailView) {
        this.mColorRepository = checkNotNull(mColorRepository, "mColorRepository cannot be null");;
        this.mColorDetailView = checkNotNull(mColorDetailView, "mColorDetailView cannot be null!");
        this.mColors = colors;
        mColorDetailView.setPresenter(this);
    }

    @Override
    public void start() {

        showColorsDetails();

    }

    private void showColorsDetails() {

        checkNotNull(mColors);

        if (mColors.size() > 0) {

            Color colorAbove = mColors.get(0);

            if (colorAbove != null) {
                mColorDetailView.showColorsDetailsAbove(colorAbove);
            }

            Color colorSelected = mColors.get(1);

            if (colorSelected != null) {
                mColorDetailView.showColorsDetails(colorSelected);
            }

            Color colorBelow = mColors.get(2);

            if (colorBelow != null) {
                mColorDetailView.showColorsDetailsBelow(colorBelow);
            }
        }

    }
}
