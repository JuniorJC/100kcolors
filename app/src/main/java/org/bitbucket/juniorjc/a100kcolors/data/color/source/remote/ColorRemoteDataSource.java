package org.bitbucket.juniorjc.a100kcolors.data.color.source.remote;

import android.support.annotation.NonNull;

import org.bitbucket.juniorjc.a100kcolors.data.color.Color;
import org.bitbucket.juniorjc.a100kcolors.data.color.source.ColorDataSource;

/**
 * Created by jcjunior on 08/09/2017.
 */

public class ColorRemoteDataSource implements ColorDataSource {

    private static final String TAG = ColorRemoteDataSource.class.getSimpleName();

    private static ColorRemoteDataSource INSTANCE;

    public static ColorDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ColorRemoteDataSource();
        }
        return INSTANCE;
    }

    // Prevent direct instantiation.
    private ColorRemoteDataSource() {}

    @Override
    public void saveColor(@NonNull Color color, SaveColorCallback callback) {
        // NOT AVAILABLE YET
    }

    @Override
    public void getColors(int pageNumber, @NonNull GetColorsCallback callback) {
        // NOT AVAILABLE YET
    }
}
