package org.bitbucket.juniorjc.a100kcolors.colors;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.bitbucket.juniorjc.a100kcolors.R;
import org.bitbucket.juniorjc.a100kcolors.data.color.Color;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import util.Constants;
import util.EndlessScrollListener;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A fragment representing a list of Items.
 * <p/>
 */
public class ColorsFragment extends Fragment implements ColorsContract.View {

    @BindView(R.id.list)
    RecyclerView recyclerView;

    private ColorAdapter mColorAdapter;

    private ColorsContract.Presenter mPresenter;

    private EndlessScrollListener scrollListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ColorsFragment() {
    }

    public static ColorsFragment newInstance() {
        return new ColorsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mColorAdapter = new ColorAdapter(new ArrayList<Color>(), getActivity());
    }

    @SuppressWarnings("unchecked")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_color_list, container, false);
        ButterKnife.bind(this, view);

        mPresenter.start();

        if (savedInstanceState == null) {
            mPresenter.loadFirstDataFromApi();
        } else {
            List<Color> colors = (List<Color>) savedInstanceState.getSerializable(Constants.COLORS_LIST);
            Parcelable listState = savedInstanceState.getParcelable(Constants.ITEM_LAST_POSITION);
            updateAdapter(colors);
            recyclerView.getLayoutManager().onRestoreInstanceState(listState);
        }

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        Parcelable listState = recyclerView.getLayoutManager().onSaveInstanceState();
        outState.putParcelable(Constants.ITEM_LAST_POSITION, listState);
        outState.putSerializable(Constants.COLORS_LIST, (Serializable) mColorAdapter.getValues());

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setPresenter(ColorsContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void updateAdapter(final List<Color> colors) {
        mColorAdapter.addData(colors);
        int positionStart = colors.get(0).getAutoIncrement();
        int count = colors.size();
        mColorAdapter.notifyItemRangeInserted(positionStart, count);
    }

    @Override
    public void showErrorLoadingColors() {
        Toast.makeText(getActivity(), "An error occurred trying to load the initial data", Toast.LENGTH_LONG).show();
    }

    @Override
    public void configureRecycleView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        if (scrollListener == null){

            scrollListener = new EndlessScrollListener(linearLayoutManager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                    if (page > 0) {
                        mPresenter.loadNextDataFromApi(page, totalItemsCount);
                    }

                }
            };
            // Adds the scroll listener to RecyclerView
            recyclerView.addOnScrollListener(scrollListener);

        }

    }

    @Override
    public void configureAdapter() {
        recyclerView.setAdapter(mColorAdapter);
    }
}
