package org.bitbucket.juniorjc.a100kcolors.colorDetails;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import org.bitbucket.juniorjc.a100kcolors.Injection;
import org.bitbucket.juniorjc.a100kcolors.R;
import org.bitbucket.juniorjc.a100kcolors.data.color.Color;

import java.util.Arrays;
import java.util.List;

import util.ActivityUtils;
import util.Constants;

public class ColorDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_details);

        Color color = (Color) getIntent().getExtras().getSerializable(Constants.COLOR_PARAM);
        Color colorAbove = (Color) getIntent().getExtras().getSerializable(Constants.COLOR_ABOVE_PARAM);
        Color colorBelow = (Color) getIntent().getExtras().getSerializable(Constants.COLOR_BELOW_PARAM);

        List<Color> colors =  Arrays.asList(colorAbove, color, colorBelow);

        Arrays.asList(colorAbove, color, colorBelow);

        ColorDetailFragment colorDetailFragment = (ColorDetailFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);

        if (colorDetailFragment == null) {
            colorDetailFragment = ColorDetailFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    colorDetailFragment, R.id.contentFrame);
        }

        new ColorDetailsPresenter(colors,
                Injection.provideColorsRepository(getApplicationContext()), colorDetailFragment);
    }
}
