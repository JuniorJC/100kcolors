package org.bitbucket.juniorjc.a100kcolors.colorDetails;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


import org.bitbucket.juniorjc.a100kcolors.R;
import org.bitbucket.juniorjc.a100kcolors.data.color.Color;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.google.common.base.Preconditions.checkNotNull;

public class ColorDetailFragment extends Fragment implements ColorDetailsContract.View {

    public static final String TAG = ColorDetailFragment.class.getSimpleName();

    @BindView(R.id.colorAboveLayout)
    RelativeLayout colorAboveLayout;

    @BindView(R.id.txtIdColorAbove)
    TextView txtIdColorAbove;

    @BindView(R.id.txtNumberColorAbove)
    TextView txtNumberColorAbove;

    @BindView(R.id.colorLayout)
    RelativeLayout colorLayout;

    @BindView(R.id.txtIdColor)
    TextView txtIdColor;

    @BindView(R.id.txtNumberColor)
    TextView txtNumberColor;

    @BindView(R.id.colorBelowLayout)
    RelativeLayout colorBelowLayout;

    @BindView(R.id.txtIdColorBelow)
    TextView txtIdColorBelow;

    @BindView(R.id.txtNumberColorBelow)
    TextView txtNumberColorBelow;

    private ColorDetailsContract.Presenter mPresenter;

    public ColorDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment ColorDetailFragment.
     */
    public static ColorDetailFragment newInstance() {
        return new ColorDetailFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_color_detail, container, false);
        ButterKnife.bind(this, rootView);

        mPresenter.start();

        return rootView;
    }

    @Override
    public void setPresenter(ColorDetailsContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }


    @Override
    public void showColorsDetails(Color color) {
        colorLayout.setBackgroundColor(android.graphics.Color.parseColor(color.getColorsInHex()));
        txtIdColor.setText(String.valueOf(color.getAutoIncrement()));
        txtNumberColor.setText(String.valueOf(color.getDivBy()));
    }

    @Override
    public void showColorsDetailsAbove(Color color) {
        colorAboveLayout.setBackgroundColor(android.graphics.Color.parseColor(color.getColorsInHex()));
        txtIdColorAbove.setText(String.valueOf(color.getAutoIncrement()));
        txtNumberColorAbove.setText(String.valueOf(color.getDivBy()));
    }

    @Override
    public void showColorsDetailsBelow(Color color) {
        colorBelowLayout.setBackgroundColor(android.graphics.Color.parseColor(color.getColorsInHex()));
        txtIdColorBelow.setText(String.valueOf(color.getAutoIncrement()));
        txtNumberColorBelow.setText(String.valueOf(color.getDivBy()));
    }
}
