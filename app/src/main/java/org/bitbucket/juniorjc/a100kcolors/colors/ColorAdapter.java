package org.bitbucket.juniorjc.a100kcolors.colors;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.bitbucket.juniorjc.a100kcolors.R;
import org.bitbucket.juniorjc.a100kcolors.colorDetails.ColorDetailsActivity;
import org.bitbucket.juniorjc.a100kcolors.data.color.Color;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import util.Constants;

public class ColorAdapter extends RecyclerView.Adapter<ColorAdapter.ViewHolder> {

    private final List<Color> mValues;
    private final Context mContext;

    public ColorAdapter(List<Color> items, Context context) {
        mValues = items;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_color, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Color color = mValues.get(position);

        holder.txtId.setText(String.valueOf(color.getAutoIncrement()));
        holder.txtNumber.setText(String.valueOf(color.getDivBy()));
        holder.mView.setBackgroundColor(android.graphics.Color.parseColor(color.getColorsInHex()));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               Intent myIntent = new Intent(mContext, ColorDetailsActivity.class);

                int listSize = mValues.size();

                // first item of the list
                if (position != 0){
                    myIntent.putExtra(Constants.COLOR_ABOVE_PARAM, mValues.get(position-1));
                }

                // last item of the list
                if (position != listSize-1){
                    myIntent.putExtra(Constants.COLOR_BELOW_PARAM, mValues.get(position+1));
                }

                // selected item of the list
                myIntent.putExtra(Constants.COLOR_PARAM, mValues.get(position));

                mContext.startActivity(myIntent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<Color> getValues() {
        return mValues;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View mView;

        @BindView(R.id.autoIncrement)
        TextView txtId;

        @BindView(R.id.divBy)
        TextView txtNumber;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mView = itemView;
        }

    }

    public void addData(List<Color> colors) {
        mValues.addAll(colors);
    }
}
