package org.bitbucket.juniorjc.a100kcolors.data.color.source;

import android.support.annotation.NonNull;

import org.bitbucket.juniorjc.a100kcolors.data.color.Color;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 *  Concrete implementation to load colors from the data sources into a cache.
 *
 * Created by jcjunior on 08/09/2017.
 */

public class ColorRepository implements ColorDataSource {

    private static final String TAG = ColorRepository.class.getSimpleName();

    private static ColorRepository INSTANCE = null;

    private final ColorDataSource mColorsRemoteDataSource;

    private final ColorDataSource mColorsLocalDataSource;

    // Prevent direct instantiation.
    private ColorRepository(@NonNull ColorDataSource mColorsRemoteDataSource,
                               @NonNull ColorDataSource mColorsLocalDataSource) {
        this.mColorsRemoteDataSource = checkNotNull(mColorsRemoteDataSource);
        this.mColorsLocalDataSource = checkNotNull(mColorsLocalDataSource);
    }

    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param mColorsRemoteDataSource the backend data source
     * @param mColorsLocalDataSource  the device storage data source
     * @return the {@link ColorRepository} instance
     */
    public static ColorRepository getInstance(ColorDataSource mColorsRemoteDataSource,
                                              ColorDataSource mColorsLocalDataSource) {
        if (INSTANCE == null) {
            INSTANCE = new ColorRepository(mColorsRemoteDataSource, mColorsLocalDataSource);
        }
        return INSTANCE;
    }

    /**
     * Used to force {@link #getInstance(ColorDataSource, ColorDataSource)} to create a new instance
     * next time it's called.
     */
    public static void destroyInstance() {
        INSTANCE = null;
    }


    @Override
    public void saveColor(@NonNull Color color, SaveColorCallback callback) {

        checkNotNull(color);
        mColorsLocalDataSource.saveColor(color, callback);

    }

    @Override
    public void getColors(final int pageNumber, @NonNull final GetColorsCallback callback) {

        checkNotNull(callback);

        // Load from server/persisted if needed.

        // Is the colors in the local data source? If not, query the network.
        mColorsLocalDataSource.getColors(pageNumber, new GetColorsCallback() {
            @Override
            public void onSuccess(List<Color> colors) {

                if (colors.isEmpty()){

                    mColorsRemoteDataSource.getColors(pageNumber, new GetColorsCallback() {
                        @Override
                        public void onSuccess(List<Color> colors) {

                            //callback to the method that called
                            callback.onSuccess(colors);
                        }

                        @Override
                        public void onError() {
                            callback.onError();
                        }

                    });

                } else {

                    callback.onSuccess(colors);
                }
            }

            @Override
            public void onError() {
                callback.onError();
            }


        });
    }
}
