package org.bitbucket.juniorjc.a100kcolors.colors;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.bitbucket.juniorjc.a100kcolors.Injection;
import org.bitbucket.juniorjc.a100kcolors.R;

import util.ActivityUtils;

public class ColorsActivity extends AppCompatActivity {

    private static final String TAG = ColorsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ColorsFragment colorsFragment = (ColorsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);

        if (colorsFragment == null) {
            colorsFragment = ColorsFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    colorsFragment, R.id.contentFrame);
        }

        new ColorsPresenter(Injection.provideColorsRepository(this), colorsFragment);
    }
}
