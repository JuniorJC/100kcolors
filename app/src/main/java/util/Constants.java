package util;

/**
 * Created by jcjunior on 08/09/2017.
 */

public class Constants {

    /**
     * Background colors of the list items
     */
    public static final String RED = "#D32F2F";
    public static final String BLUE = "#448AFF";
    public static final String YELLOW = "#FBC02D";

    /**
     * Number of items that should be displayed
     */
    public static final Integer INDEX_LIMIT = 100000;

    /**
     * Parameters of activities
     */
    public static final String COLOR_PARAM = "color";
    public static final String COLOR_ABOVE_PARAM = "color_above";
    public static final String COLOR_BELOW_PARAM = "color_below";
    public static final String ITEM_LAST_POSITION = "last_position";
    public static final String COLORS_LIST = "color_list";

    /**
     * Number of items that should be returned after pagination
     */
    public static final int ITENS_PER_PAGE = 20;

}
